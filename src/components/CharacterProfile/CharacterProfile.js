import React from "react";

import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

import Image from "react-bootstrap/Image";
import banner from "../../banner.jpg";
  
  class CharacterProfile extends React.Component{
      

      state = {
          location: []
      }
      async componentDidMount(){
        const character = this.props.location.state.character
        const url = character.location.url
        const response =  await fetch(url).then(resp => resp.json());
        let loc = response;
        this.setState({
            location: loc
        })
        
      }
     
      render(){
        const character = this.props.location.state.character
        const loc = this.state.location;
      
  return (
    <div className="profile-top-div">
    <div>
    <header className="header">
    <Image src={banner} fluid />
  </header>
  </div>
    <div className="character-profile">
    <div className="profile-top">
      <img className="profile-image" src={character.image} alt="character"></img>
      <h3>Name: {character.name}</h3>
      <h4>Status: {character.status}</h4>
      </div>
      <div className="profile-text">
      <h4>Profile</h4>
      <p>Gender: {character.gender}</p>
      <p>Species: {character.species}</p>
      <p>Origin: {character.origin.name}</p>
      <h4>Location</h4>
      <p>Current Location: {loc.name}</p>
      <p>Type: {loc.type}</p>
      <p>Dimension: {loc.dimension}</p>
      <div>
      <Link to={{ pathname: "/" }}><Button className="linkback" variant="primary">Back</Button></Link>
      </div>
      </div>
    </div>
    </div>
  );
};}
export default CharacterProfile;
