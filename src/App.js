import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import CharacterProfile from "./components/CharacterProfile/CharacterProfile";
import MainPage from "./components/MainPage/MainPage";



class App extends React.Component {
  render() {
    return (
      <Router>
      <div>
        <Switch>
          <Route exact path="/" component={MainPage} />
          <Route path="/profile" component={CharacterProfile} />
        </Switch>
      </div>
      </Router>
    );
  }
}
export default App;
